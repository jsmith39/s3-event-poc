import json
from aws_lambda_powertools.utilities.data_classes import event_source, S3Event
import boto3


s3_client = boto3.client('s3')

@event_source(data_class=S3Event)
def lambda_handler(event, context):
    print ("hello world from S3")
    for record in event.records:
        print (f"{event.bucket_name}/{record.s3.get_object.key}")
        response = s3_client.get_object(Bucket=event.bucket_name, Key=record.s3.get_object.key)
        print(response['Body'].read().decode())


